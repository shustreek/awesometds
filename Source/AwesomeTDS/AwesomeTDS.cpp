// Copyright Epic Games, Inc. All Rights Reserved.

#include "AwesomeTDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AwesomeTDS, "AwesomeTDS" );

DEFINE_LOG_CATEGORY(LogAwesomeTDS)
 