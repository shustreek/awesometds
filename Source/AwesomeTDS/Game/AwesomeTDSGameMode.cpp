// Copyright Epic Games, Inc. All Rights Reserved.

#include "AwesomeTDSGameMode.h"
#include "AwesomeTDSPlayerController.h"
#include "AwesomeTDS/Character/AwesomeTDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAwesomeTDSGameMode::AAwesomeTDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AAwesomeTDSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}