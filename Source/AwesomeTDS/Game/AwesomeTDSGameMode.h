// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AwesomeTDSGameMode.generated.h"

UCLASS(minimalapi)
class AAwesomeTDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAwesomeTDSGameMode();
};



